﻿using UI.Common.Components;
using UI.Common.Data;
using UnityEngine;

namespace Model
{
    public class SettingViewModel
    {
        static object _lock = new object();
        static Texture2D _prototype;
        public static Texture2D TileImage
        {
            get
            {
                lock (_lock)
                {
                    return _prototype;
                }
            }
            set
            {
                lock (_prototype)
                {
                    _prototype = value;
                }
            }
        }

        public static Color SelectedColor { get; internal set; }
        public static ObjectPrototype PrototypeSelected { get; internal set; }
    }
}