﻿namespace Assets.Model
{
    public class NavigationViewModel
    {
        public delegate void SelectedIndexChanged(int index);
        public static SelectedIndexChanged OnSelectedIndexChanged;

        static int _selectedIndex;
        public static int SelectedIndex
        {
            get { return _selectedIndex; }
            set { SetSelectedIndex(value); }
        }

        static void SetSelectedIndex(int value)
        {
            if (_selectedIndex != value)
            {
                _selectedIndex = value;
                DispatchOnSelectedIndexChanged();
            }
        }

        static void DispatchOnSelectedIndexChanged()
        {
            if (OnSelectedIndexChanged != null)
            {
                OnSelectedIndexChanged(SelectedIndex);
            }
        }
    }
}