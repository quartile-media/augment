﻿using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using UI.Common.Components;
using UI.Common.Data;
using UI.Common.Views;
using UI.Home.Views;
using UnityEngine;

namespace UI.Orientation
{
    public class DynamicOrientationSwitcher : UIComponent
    {
        #region Skin parts

        /// <summary>
        /// Prefabs of the canvas
        /// </summary>
        public DynamicOrientationCanvas[] CanvasPrototypes;

        public View ViewPrototype;

        public ViewsConfiguration Configuration;

        #endregion

        #region Properties

        ScreenOrientation _orientation = ScreenOrientation.Portrait;
        /// <summary>
        /// Orientation
        /// </summary>
        public ScreenOrientation Orientation
        {
            get { return _orientation; }
            private set
            {
                SetOrientation(value);
            }
        }

        DynamicOrientationCanvas instance;

        RectTransform rectTransform;

        #endregion

        #region Accessors

        DynamicOrientationCanvas GetCanvasProtoype(ScreenOrientation orientation)
        {
            try
            {
                return CanvasPrototypes.First(canvas => canvas.ActiveOrientations.Contains(orientation));
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region Mutators

        public void SetOrientation(ScreenOrientation value)
        {
            if (_orientation != value)
            {
                _orientation = value;
                Invalidate();
            }
        }

        #endregion

        #region Lifecycle

        protected override void CollectComponents()
        {
            base.CollectComponents();

            rectTransform = GetComponentInChildren<RectTransform>();
        }

        protected void LateUpdate()
        {
            Process(rectTransform);
        }

        private void Process(RectTransform rectTransform)
        {
            try
            {
                float aspect = rectTransform.sizeDelta.x / rectTransform.sizeDelta.y;
                bool isLandscape = aspect >= 1;

                Orientation = (isLandscape) ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;
            }
            catch
            {
                Orientation = ScreenOrientation.Portrait;
            }
        }

        protected override void CommitProperties()
        {
            UpdateOrientation();

            base.CommitProperties();
        }

        #endregion

        #region Orientation

        void UpdateOrientation()
        {
            UpdateOrientation(Orientation);
        }

        void UpdateOrientation(ScreenOrientation orientation)
        {
            if (instance != null)
            {
                DestroyImmediate(instance.gameObject);
            }

            var prototype = GetCanvasProtoype(orientation);
            if (prototype != null)
            {
                instance = Instantiate(prototype, transform);
                instance.ViewPrototype = ViewPrototype;
            }

            if (ViewPrototype is ContainerPageView)
            {
                (ViewPrototype as ContainerPageView).ViewsConfiguration = Configuration;
            }
        }

        #endregion
    }
}
