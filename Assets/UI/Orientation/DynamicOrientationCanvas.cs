﻿using System;
using UI.Common.Components;
using UI.Common.Views;
using UI.Home.Views;
using UnityEngine;

namespace UI.Orientation
{
    public class DynamicOrientationCanvas : UIComponent
    {
        #region Properties

        /// <summary>
        /// List of orientations in which this canvas should be used
        /// </summary>
        public ScreenOrientation[] ActiveOrientations;

        private View viewPrototype;
        private ViewContainer ViewContainer;

        public View ViewPrototype
        {
            get { return viewPrototype; }
            set
            {
                SetPrototype(value);
            }
        }

        #endregion

        protected override void CommitProperties()
        {
            base.CommitProperties();
            UpdateUI();
        }

        protected override void CollectComponents()
        {
            ViewContainer = GetComponentInChildren<ViewContainer>();
        }

        private void UpdateUI()
        {
            if (viewPrototype != null && ViewContainer != null)
            {
                var instance = Instantiate(ViewPrototype, ViewContainer.transform);
                if (instance is ContainerPageView)
                    (instance as ContainerPageView).ViewsConfiguration = (ViewPrototype as ContainerPageView).ViewsConfiguration;
            }
        }

        private void SetPrototype(View value)
        {
            if (value != viewPrototype)
            {
                viewPrototype = value;
                Invalidate();
            }
        }
    }
}