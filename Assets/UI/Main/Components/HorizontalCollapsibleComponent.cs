﻿using UI.Common.Components;
using UI.Common.Types;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace Assets.UI.Main.Components
{
    [RequireComponent(typeof(LayoutElement))]
    public class HorizontalCollapsibleComponent : ExpandableComponent
    {
        #region Skin parts

        [HideInInspector] protected LayoutElement ExpandableObject;
        #endregion

        #region Lifecycle

        protected override void CollectComponents()
        {
            base.CollectComponents();
            this.ExpandableObject = GetComponent<LayoutElement>();
        }

        #endregion

        #region

        protected override void CommitProperties()
        {
            UpdateUI();
        }

        void UpdateUI()
        {
            UpdateUI(this.State);
        }

        private void UpdateUI(DrawerState state)
        {
            if (state == DrawerState.Open)
            {
                Expand();
                return;
            }

            Collapse();
        }

        public void Expand()
        {
            if (ExpandableObject != null)
            {
                ExpandableObject.DOMinSize(new Vector2(ExpandedWidth, ExpandedHeight), 1, false);
                ExpandableObject.DOPreferredSize(new Vector2(ExpandedWidth, ExpandedHeight), 1, false).OnComplete(OnPlayedAnimationAction);
            }
        }

        public void Collapse()
        {
            if (ExpandableObject != null)
            {
                ExpandableObject.DOMinSize(new Vector2(CollapsedWidth, ExpandedHeight), 1, false);
                ExpandableObject.DOPreferredSize(new Vector2(CollapsedWidth, ExpandedHeight), 1, false).OnComplete(OnPlayedAnimationAction);
            }
        }

        #endregion
    }
}
