﻿using UI.Common.Components;
using UI.Common.Views;
using UnityEngine.SceneManagement;

namespace Assets.UI.Main.Views
{
    public class FurnitureView : View
    {
        private SimpleButton button;
        protected override void CollectComponents()
        {
            button = GetComponentInChildren<SimpleButton>();
        }

        public override void AddListeners()
        {
            if (button != null)
            {
                button.OnClick += onSettingButtonClicked;
            }
        }

        private void onSettingButtonClicked()
        {
            SceneManager.LoadScene("Setting");
        }
    }
}