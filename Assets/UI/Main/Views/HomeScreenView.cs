﻿using Assets.UI.Main.Components;
using UI.Common.Components;
using UI.Common.Types;
using UI.Common.Views;

namespace UI.Main.Views
{
    public class HomeScreenView : InvalidatableView
    {
        private HorizontalCollapsibleComponent expandablePanel;
        private SimpleButton button;
        protected override void CollectComponents()
        {
            base.CollectComponents();
            this.expandablePanel = GetComponentInChildren<HorizontalCollapsibleComponent>();
            this.button = GetComponentInChildren<SimpleButton>();
        }

        public override void AddListeners()
        {
            if (button != null)
            {
                button.OnClick += HandleOnClick;
            }
        }

        private void HandleOnClick()
        {
            expandablePanel.State = expandablePanel.State == DrawerState.Close ? DrawerState.Open : DrawerState.Close;
        }

        public override void RemoveListeners()
        {
            if (button != null)
            {
                button.OnClick -= HandleOnClick;
            }
        }
    }
}
