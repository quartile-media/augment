﻿using UI.Common.Components;
using UnityEngine;

namespace UI.Common.Data
{
    [CreateAssetMenu(fileName = "ObjectPrototype", menuName = "RabbitEar/UI/Common/Data/Prototype", order = 1)]
    public class ObjectPrototype : ScriptableObject
    {
        public string Heading;
        public string Discription;
        public ObjectPrototypeComponent Prototype;
    }
}