﻿using UnityEngine;

namespace UI.Common.Data
{
    [CreateAssetMenu(fileName = "ObjectPrototype", menuName = "RabbitEar/UI/Common/Data/Menu configuration", order = 1)]
    public class MenuConfiguration : ScriptableObject
    {
        public string Heading;
        public ObjectPrototype[] Prototypes;
    }
}
