﻿using UI.Common.Views;
using UnityEngine;

namespace UI.Common.Data
{
    [CreateAssetMenu(fileName = "ViewConfiguration", menuName = "RabbitEar/UI/Common/Data/View configuration", order = 1)]
    public class ViewConfiguration : ScriptableObject
    {
        public Texture Image;
        public View ViewPrototype;
        public bool ExternalScene;
        public string SceneName;
    }
}
