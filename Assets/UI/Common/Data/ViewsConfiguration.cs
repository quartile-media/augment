﻿using UI.Common.Views;
using UnityEngine;

namespace UI.Common.Data
{
    [CreateAssetMenu(fileName = "ViewConfiguration", menuName = "RabbitEar/UI/Common/Data/Application views configuration", order = 1)]
    public class ViewsConfiguration : ScriptableObject
    {
        public ViewConfiguration[] Views;
        public HomeMenuButtonComponent buttonPrototype;
        public bool RenderViews;
    }
}