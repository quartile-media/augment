﻿using UnityEngine;

namespace UI.Common.Types
{
    public class SystemColor
    {
        public Color Color { get; set; }
        public string Name { get; set; }

        public SystemColor(Color color, string name)
        {
            Color = color;
            Name = name;
        }
    }
}
