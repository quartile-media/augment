﻿namespace UI.Common.Types
{
    public enum DrawerState
    {
        Open = 0,
        Close = 1
    }

    public enum SwitchState
    {
        Off,
        On
    }
}
