﻿using UI.Common.Types;

namespace UI.Common.Utils
{
    public class DrawerUtils
    {
        public static SwitchState GetSwitchState(DrawerState state)
        {
            return state == DrawerState.Open ? SwitchState.On : SwitchState.Off;
        }

        public static DrawerState GetDrawerState(SwitchState state)
        {
            return state == SwitchState.On ? DrawerState.Open : DrawerState.Close;
        }
    }
}
