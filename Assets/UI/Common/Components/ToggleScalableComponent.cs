﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UI.Common.Components
{
    /// <summary>
    /// ToggleScalableComponent
    /// </summary>
    public class ToggleScalableComponent : UIComponent
    {
        #region Properties

        /// <summary>
        /// OffScale
        /// </summary>
        public float OffScale;
        /// <summary>
        /// OnScale
        /// </summary>
        public float OnScale;

        float _value;
        /// <summary>
        /// Value
        /// </summary>
        public float Value
        {
            get { return _value; }
            set
            {
                SetValue(value);
            }
        }

        #endregion

        #region Accessors

        float GetScale()
        {
            return GetScale(Value);
        }

        float GetScale(float t)
        {
            try
            {
                return Mathf.Lerp(OffScale, OnScale, t);
            }
            catch
            {
                return 0;
            }
        }

        #endregion

        #region Mutators

        private void SetValue(float value)
        {
            if (_value != value)
            {
                _value = value;
                Invalidate();
            }
        }

        #endregion

        #region Lifecycle

        protected override void CommitProperties()
        {
            UpdateUI(transform);
            base.CommitProperties();
        }

        #endregion

        #region UI

        void UpdateUI(Transform transform)
        {
            if (transform != null)
            {
                transform.localScale = Vector3.one * GetScale();
            }
        }

        #endregion
    }
}
