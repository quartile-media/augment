﻿
using UnityEngine;
using UnityEngine.UI;

namespace UI.Common.Components
{
    [RequireComponent(typeof(Button))]
    public class SimpleButton : UIComponent
    {
        #region Events

        /// <summary>
        /// Delegate to handle Click
        /// </summary>
        public delegate void Click();
        /// <summary>
        /// Dispatched when Click
        /// </summary>
        public event Click OnClick;

        #endregion

        #region Skin parts

        /// <summary>
        /// Click handler
        /// </summary>
        Button Button;

        #endregion

        #region Lifecycle

        protected override void CollectComponents()
        {
            base.CollectComponents();

            Button = GetComponent<Button>();
        }

        #endregion

        #region Listeners

        public override void AddListeners()
        {
            base.AddListeners();

            AddListeners(Button);
        }

        void AddListeners(Button button)
        {
            if (button != null)
            {
                button.onClick.AddListener(HandleOnClick);
            }
        }

        public override void RemoveListeners()
        {
            base.RemoveListeners();

            RemoveListeners(Button);
        }

        private void RemoveListeners(Button button)
        {
            if (button != null)
            {
                button.onClick.RemoveListener(HandleOnClick);
            }
        }

        #endregion

        #region Handlers

        void HandleOnClick()
        {
            DispatchOnClick();
        }

        #endregion

        #region Dispatchers

        void DispatchOnClick()
        {
            if (OnClick != null)
                OnClick();
        }

        #endregion
    }
}
