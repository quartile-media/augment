﻿using System;
using DG.Tweening;
using UI.Common.Types;
using UnityEngine;

namespace UI.Common.Components
{
    public class ExpandableComponent : UIComponent
    {
        #region Skin parts

        /// <summary>
        /// CollapsedWidth
        /// </summary>
        [SerializeField] protected float CollapsedWidth;

        /// <summary>
        /// ExpandedWidth
        /// </summary>
        [SerializeField] protected float ExpandedWidth;

        /// <summary>
        /// CollapsedHeight
        /// </summary>
        [SerializeField] protected float CollapsedHeight;

        /// <summary>
        /// ExpandedHeight
        /// </summary>
        [SerializeField] protected float ExpandedHeight;

        /// <summary>
        /// expandDuration
        /// </summary>
        [SerializeField] public float ExpandDuration = 0.5f;

        /// <summary>
        /// CollapseDuration
        /// </summary>
        [SerializeField] public float CollapseDuration = 0.5f;



        #endregion

        #region Properties

        /// <summary>
        /// State
        /// </summary>
        private DrawerState _state;
        public DrawerState State
        {
            get { return _state; }
            set { SetDrawerState(value); }
        }

        public TweenCallback OnPlayedAnimationAction { get; set; }

        public TweenCallback BeforePlayingAnimationAction { get; set; }

        #endregion

        #region Mutators
        private void SetDrawerState(DrawerState value)
        {
            _state = value;
            Invalidate();
        }
       
        #endregion
    }
}
