﻿using UnityEngine;

namespace UI.Common.Components
{
    public class UIComponent : MonoBehaviour
    {
        public void Start()
        {
            this.OnStart();
            this.CollectComponents();
            this.AddListeners();
            this.Invalidate();
        }

        protected virtual void CollectComponents()
        {
            // Do nothing.
        }

        protected virtual void CommitProperties()
        {
            // Do nothing.
        }

        protected virtual void OnStart()
        {
            // Do nothing.
        }

        public virtual void AddListeners()
        {
            // Do nothing.
        }

        public virtual void RemoveListeners()
        {
            // Do nothing.
        }

        protected void Invalidate()
        {
            CommitProperties();
        }
    }
}
