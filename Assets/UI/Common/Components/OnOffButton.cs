﻿using UI.Common.Components;
using UI.Common.Types;

namespace Assets.UI.Common.Components
{
    public class OnOffButton : UIComponent
    {
        #region Events

        /// <summary>
        /// Delegate to handle changes in on/off switch state
        /// </summary>
        /// <param name="state"></param>
        public delegate void StateChangeChanged(SwitchState state);
        /// <summary>
        /// Dispatched when on/off switch state changes
        /// </summary>
        public event StateChangeChanged OnStateChanged;

        #endregion

        #region Skin parts

        /// <summary>
        /// SimpleButtonComponent
        /// </summary>
        SimpleButton SimpleButtonComponent;

        /// <summary>
        /// iconToggle
        /// </summary>
        SimpleToggleComponent iconToggle;

        private SwitchState state;

        #endregion

        #region Properties


        /// <summary>
        /// State
        /// </summary>
        public SwitchState State
        {
            get { return state; }
            set
            {
                SetState(value);
            }
        }

        /// <summary>
        /// IsInteractive
        /// </summary>
        public bool IsInteractive = true;

        #endregion

        #region Accessors

        bool GetIsOn(SwitchState state)
        {
            return state == SwitchState.On;
        }

        SwitchState GetState(bool isOn)
        {
            return isOn ? SwitchState.On : SwitchState.Off;
        }

        #endregion

        #region Mutators

        void SetState(SwitchState value)
        {
            state = value;
            DispatchOnStateChanged(value);
            Invalidate();
        }


        #endregion

        #region Lifecycle

        protected override void CollectComponents()
        {
            base.CollectComponents();

            SimpleButtonComponent = GetComponentInChildren<SimpleButton>();
            iconToggle = GetComponentInChildren<SimpleToggleComponent>();
        }

        protected override void CommitProperties()
        {
            base.CommitProperties();

            UpdateUI();
        }

        void UpdateUI()
        {
            UpdateUI(iconToggle);
        }

        void UpdateUI(SimpleToggleComponent autoSliderComponent)
        {
            if (autoSliderComponent != null)
            {
                autoSliderComponent.Value = (state == SwitchState.On) ? 1 : 0;
            }
        }

        void ToggleState()
        {
            // Can be extended to support tristate bool.
            State = State == SwitchState.On ? SwitchState.Off : SwitchState.On;
        }

        #endregion

        #region Listeners

        public override void AddListeners()
        {
            base.AddListeners();

            AddListeners(SimpleButtonComponent);
        }

        private void AddListeners(SimpleButton button)
        {
            if (button != null)
            {
                button.OnClick += HandleButtonOnClick;
            }
        }

        public override void RemoveListeners()
        {
            base.RemoveListeners();

            RemoveListeners(SimpleButtonComponent);
        }

        private void RemoveListeners(SimpleButton button)
        {
            if (button != null)
            {
                button.OnClick -= HandleButtonOnClick;
            }
        }

        #endregion

        #region Handlers

        private void HandleButtonOnClick()
        {
            if (IsInteractive)
                ToggleState();
        }

        #endregion

        #region Dispatchers

        void DispatchOnStateChanged(SwitchState state)
        {
            if (OnStateChanged != null)
                OnStateChanged(state);
        }

        #endregion
    }
}
