﻿
using System.Collections;
using UnityEngine;

namespace UI.Common.Components
{
    public class SimpleToggleComponent : UIComponent
    {
        #region Skin parts

        /// <summary>
        /// Images that need to be tinted
        /// </summary>
        TintableImageComponent[] Tintables;
        /// <summary>
        /// Images that need to be scaled
        /// </summary>
        ToggleScalableComponent[] Scalables;

        #endregion

        #region Properties

        float _value;
        /// <summary>
        /// Value
        /// </summary>
        public float Value
        {
            get { return _value; }
            set
            {
                SetValue(value);
            }
        }

        public float StartDelay = 0;
        public float Duration = 1;
        public AnimationCurve Easing = AnimationCurve.Linear(0, 0, 1, 1);

        float valueStart;
        float valueEnd;
        float valueDelta;
        bool IsAnimating = false;
        float startTime;
        float endTime;
        Coroutine animationCoroutine;

        #endregion

        #region Mutators

        void SetValue(float value)
        {
            if (_value != value)
            {
                StopAnimation();
                AnimateTo(value);
            }
            else
            {
                valueStart = valueEnd = _value;
                valueDelta = 0;
                AnimationUpdated(1);
            }
        }

        #endregion

        #region Lifecycle

        protected override void CollectComponents()
        {
            base.CollectComponents();

            Tintables = GetComponentsInChildren<TintableImageComponent>();
            Scalables = GetComponentsInChildren<ToggleScalableComponent>();
        }

        protected override void CommitProperties()
        {
            UpdateUI();
            base.CommitProperties();
        }

        #endregion

        #region UI

        protected virtual void UpdateUI()
        {
            UpdateUI(Tintables);
            UpdateUI(Scalables);
        }

        #endregion

        #region Tintables

        void UpdateUI(TintableImageComponent[] tintables)
        {
            if (tintables != null && tintables.Length > 0)
            {
                foreach (var tintable in tintables)
                {
                    UpdateUI(tintable);
                }
            }
        }

        void UpdateUI(TintableImageComponent tintable)
        {
            if (tintable != null)
            {
                tintable.Value = Value;
            }
        }

        #endregion

        #region Scalables

        void UpdateUI(ToggleScalableComponent[] scalables)
        {
            if (scalables != null && scalables.Length > 0)
            {
                foreach (var scalable in scalables)
                {
                    UpdateUI(scalable);
                }
            }
        }

        void UpdateUI(ToggleScalableComponent scalable)
        {
            if (scalable != null)
            {
                scalable.Value = Value;
            }
        }

        #endregion

        #region Animation

        void AnimateTo(float value)
        {
            if (gameObject.activeInHierarchy && !IsAnimating)
            {
                valueStart = Value;
                valueEnd = value;
                valueDelta = valueEnd - valueStart;

                SetTimings();

                AnimationStarting();
                animationCoroutine = StartCoroutine(AnimationThread());
            }
        }

        public virtual void StopAnimation()
        {
            if (animationCoroutine != null)
            {
                StopCoroutine(animationCoroutine);
            }
            AnimationStopped();
        }

        private void SetTimings()
        {
            startTime = Time.time + StartDelay;
            endTime = startTime + Duration;
        }

        protected virtual IEnumerator AnimationThread()
        {
            if (StartDelay > 0)
            {
                AnimationUpdated(0);
                yield return new WaitForSeconds(StartDelay);
            }

            AnimationStarted();

            while (Time.time < endTime)
            {
                float currentTime = Time.time;
                float elapsedTime = currentTime - startTime;
                float progress = Mathf.Clamp01(elapsedTime / Duration);
                float easedProgress = Easing.Evaluate(progress);

                AnimationUpdated(easedProgress);

                yield return new WaitForEndOfFrame();
            }

            AnimationComplete();
        }

        protected virtual void AnimationStarting()
        {
            IsAnimating = true;
            AnimationUpdated(0);
        }

        protected virtual void AnimationStarted()
        {
            // template method
        }

        protected virtual void AnimationUpdated(float progress)
        {
            // set _value directly as we dont want to start a new animation
            _value = valueStart + (valueDelta * progress);
            // invalidate so that we update our sub components
            Invalidate();
        }

        protected virtual void AnimationComplete()
        {
            AnimationUpdated(1);
            IsAnimating = false;
        }

        protected virtual void AnimationStopped()
        {
            IsAnimating = false;
        }

        #endregion
    }
}
