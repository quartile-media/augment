﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.Common.Components
{
    /// <summary>
    /// TintableImageComponent
    /// </summary>
    [RequireComponent(typeof(Image))]
    public class TintableImageComponent : UIComponent
    {
        #region Skin parts

        /// <summary>
        /// The image we tint
        /// </summary>
        Image Image;

        #endregion

        #region Properties

        /// <summary>
        /// OffColour
        /// </summary>
        public Color32 OffColour;
        /// <summary>
        /// OnColor
        /// </summary>
        public Color32 OnColor;

        float _value;
        /// <summary>
        /// Value
        /// </summary>
        public float Value
        {
            get { return _value; }
            set
            {
                SetValue(value);
            }
        }

        Texture _texture;
        /// <summary>
        /// Texture
        /// </summary>
        public Texture Texture
        {
            get { return _texture; }
            set
            {
                SetTexture(value);
            }
        }

        #endregion

        #region Accessors

        Color32 GetColor()
        {
            return GetColor(Value);
        }

        Color32 GetColor(float t)
        {
            try
            {
                return Color.Lerp(OffColour, OnColor, t);
            }
            catch
            {
                return Color.clear;
            }
        }

        #endregion

        #region Mutators

        private void SetValue(float value)
        {
            if (_value != value)
            {
                _value = value;
                Invalidate();
            }
        }

        public void SetTexture(Texture value)
        {
            if (_texture != value)
            {
                _texture = value;
                Invalidate();
            }
        }

        public void ClearTexture()
        {
            Texture = null;
            UpdateUI(Image, null);
        }

        #endregion

        #region Lifecycle

        protected override void CollectComponents()
        {
            base.CollectComponents();

            Image = GetComponent<Image>();
        }

        protected override void CommitProperties()
        {
            UpdateUI(Image);
            base.CommitProperties();
        }

        #endregion

        #region UI

        void UpdateUI(Image image)
        {
            if (image != null)
            {
                image.color = GetColor();

                if (Texture != null)
                    UpdateUI(image, Texture);
            }
        }

        void UpdateUI(Image image, Texture texture)
        {
            if (image != null)
            {
                image.sprite = Sprite.Create(texture as Texture2D, new Rect(Vector2.zero, new Vector2(texture.width, texture.height)), Vector2.one / 2);
            }
        }

        #endregion
    }
}
