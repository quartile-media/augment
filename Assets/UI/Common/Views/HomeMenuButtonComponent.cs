﻿using System;
using UI.Common.Components;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Common.Views
{
    public class HomeMenuButtonComponent : UIComponent
    {
        public delegate void MenuItemClicked(int index);
        public MenuItemClicked OnClickMenuItemClicked;

        private Texture _texture;
        private SimpleButton ButtonComponent;
        public Texture Texture
        {
            get { return _texture; }
            set { SetTexture(value); }
        }

        public Image Icon;
        protected override void CommitProperties()
        {
            base.CommitProperties();
            UpdateUI();
        }

        protected override void CollectComponents()
        {
            ButtonComponent = GetComponentInChildren<SimpleButton>();
        }

        public override void AddListeners()
        {
            if (ButtonComponent != null)
            {
                ButtonComponent.OnClick += onSimpleButtonClick;
            }
        }

        public override void RemoveListeners()
        {
            if (ButtonComponent != null)
            {
                ButtonComponent.OnClick -= onSimpleButtonClick;
            }
        }

        private void onSimpleButtonClick()
        {
            DispatchOnClickMenuItemClicked();
        }

        private void DispatchOnClickMenuItemClicked()
        {
            if (OnClickMenuItemClicked != null)
            {
                OnClickMenuItemClicked(Index);
            }
        }

        private void UpdateUI()
        {
            UpdateUI(Icon);
        }

        private int _index;


        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }


        private void UpdateUI(Image icon)
        {
            if (icon != null)
            {
                icon.sprite = Sprite.Create(Texture as Texture2D, new Rect(Vector2.zero, new Vector2(Texture.width, Texture.height)), Vector2.one / 2);
            }
        }

        private void SetTexture(Texture value)
        {
            if (_texture != value)
            {
                _texture = value;
                Invalidate();
            }
        }

    }
}