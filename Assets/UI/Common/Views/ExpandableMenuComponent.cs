﻿using Assets.UI.Common.Components;
using Assets.UI.Main.Components;
using System;
using System.Collections;
using UI.Common.Components;
using UI.Common.Data;
using UI.Common.Types;
using UI.Common.Utils;
using UnityEngine;

namespace UI.Common.Views
{
    public class ExpandableMenuComponent : UIComponent
    {
        #region  Skin parts

        [HideInInspector]
        public HorizontalCollapsibleComponent ExpandableComponent { get; set; }
        public MenuConfiguration MenuConfiguration;
        public MenuContainer MenuArea;
        /// <summary>
        /// The collapsible handle of the drawer.
        /// </summary>
        public OnOffButton DrawerHandle;

        #endregion

        #region Properties

        DrawerState _collapsibleState = DrawerState.Close;
        /// <summary>
        /// CollapsibleState
        /// </summary>
        public DrawerState CollapsibleState
        {
            get { return _collapsibleState; }
            set
            {
                SetCollapsibleState(value);
            }
        }

        #endregion

        #region Lifecycle

        protected override void CollectComponents()
        {
            base.CollectComponents();
            this.ExpandableComponent = GetComponentInChildren<HorizontalCollapsibleComponent>();
            MenuArea = GetComponentInChildren<MenuContainer>();
            SpawnItems();
        }

        private void SpawnItems()
        {
            if (MenuConfiguration != null)
            {
                foreach (var prototype in MenuConfiguration.Prototypes)
                {
                    // Instantiate at position (0, 0, 0) and zero rotation.
                    Instantiate(prototype.Prototype, MenuArea.transform);
                }
            }
        }

        protected override void CommitProperties()
        {
            base.CommitProperties();
            UpdateUI();
        }

        void UpdateUI()
        {
            UpdateUI(CollapsibleState);
        }

        void UpdateUI(DrawerState state)
        {
            DrawerHandle.State = DrawerUtils.GetSwitchState(state);
            UpdateViewModelNavigationState(state);
        }

        void UpdateViewModelNavigationState(DrawerState state)
        {
            StartCoroutine(PerformAnimationAfterOpen(state));
            StartCoroutine(PerformAnimationBeforeClose(state));
        }

        private IEnumerator PerformAnimationAfterOpen(DrawerState state)
        {
            if (ExpandableComponent != null && state == DrawerState.Open)
                yield return new WaitForSeconds(ExpandableComponent.ExpandDuration);
        }

        private IEnumerator PerformAnimationBeforeClose(DrawerState state)
        {
            yield return null;
            if (ExpandableComponent != null)
            {
                ExpandableComponent.State = state;
            }
        }

        #endregion

        #region Listeners

        public override void AddListeners()
        {
            base.AddListeners();

            if (DrawerHandle != null)
                DrawerHandle.OnStateChanged += HandleSwitchStateChanged;
        }

        public override void RemoveListeners()
        {
            base.RemoveListeners();

            if (DrawerHandle != null)
                DrawerHandle.OnStateChanged -= HandleSwitchStateChanged;
        }

        #endregion

        #region Handlers

        private void HandleSwitchStateChanged(SwitchState state)
        {
            this.CollapsibleState = DrawerUtils.GetDrawerState(state);
        }

        #endregion

        #region Mutators

        private void SetCollapsibleState(DrawerState value)
        {
            if (_collapsibleState != value)
            {
                _collapsibleState = value;
                Invalidate();
            }
        }

        #endregion
    }
}
