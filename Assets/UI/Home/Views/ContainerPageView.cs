using Assets.Model;
using System;
using System.Linq;
using UI.Common.Components;
using UI.Common.Data;
using UI.Common.Views;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.Home.Views
{
    public class ContainerPageView : View
    {
        private ViewContainer ViewContainer;

        private NavigationContainer NavigationContainer;

        private ViewsConfiguration _viewsConfiguration;

        private View viewInstance;

        public ViewsConfiguration ViewsConfiguration
        {
            get
            {
                return this._viewsConfiguration;
            }
            set
            {
                this.SetViewConfiguration(value);
            }
        }

        public ContainerPageView()
        {
        }

        public override void AddListeners()
        {
            NavigationViewModel.OnSelectedIndexChanged += new NavigationViewModel.SelectedIndexChanged(this.onSelectedIndexChanged);
        }

        private void AddListeners(HomeMenuButtonComponent buttonInstance)
        {
            buttonInstance.OnClickMenuItemClicked += new HomeMenuButtonComponent.MenuItemClicked(this.OnClickMenuItemClicked);
        }

        protected override void CollectComponents()
        {
            this.ViewContainer = base.GetComponentInChildren<ViewContainer>();
            this.NavigationContainer = base.GetComponentInChildren<NavigationContainer>();
        }

        protected override void CommitProperties()
        {
            base.CommitProperties();
            this.UpdateUI();
        }

        private void OnClickMenuItemClicked(int index)
        {
            NavigationViewModel.SelectedIndex = index;
        }

        private void onSelectedIndexChanged(int index)
        {
            if (this.viewInstance != null)
            {
                Destroy(this.viewInstance.gameObject);
            }
            this.RenderView();
        }

        private void RenderView()
        {
            if (this.ViewsConfiguration.Views[NavigationViewModel.SelectedIndex].ExternalScene)
            {
                SceneManager.LoadScene(this.ViewsConfiguration.Views[NavigationViewModel.SelectedIndex].SceneName);
            }
            else if (this.ViewsConfiguration.RenderViews)
            {
                this.viewInstance = Instantiate<View>(this.ViewsConfiguration.Views[NavigationViewModel.SelectedIndex].ViewPrototype, this.ViewContainer.transform);
            }
        }

        private void SetViewConfiguration(ViewsConfiguration value)
        {
            if (this._viewsConfiguration != value)
            {
                this._viewsConfiguration = value;
                base.Invalidate();
            }
        }

        private void Spawn(ViewsConfiguration viewsConfiguration)
        {
            if (viewsConfiguration != null && viewsConfiguration.Views != null && viewsConfiguration.Views.Any<ViewConfiguration>())
            {
                int index = 0;
                ViewConfiguration[] views = viewsConfiguration.Views;
                for (int i = 0; i < (int)views.Length; i++)
                {
                    ViewConfiguration viewConfiguration = views[i];
                    HomeMenuButtonComponent image = Instantiate<HomeMenuButtonComponent>(viewsConfiguration.buttonPrototype, this.NavigationContainer.transform);
                    image.Texture = viewConfiguration.Image;
                    image.Index = index;
                    this.AddListeners(image);
                    index++;
                }
            }
        }

        private void UpdateUI()
        {
            this.Spawn(this.ViewsConfiguration);
            this.RenderView();
        }
    }
}