﻿using Model;
using UI.Common.Components;
using UI.Settings.Image.Data;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Settings.Image.Components
{
    public class ImagePrototypeComponent : UIComponent
    {
        public Text Label;
        public UnityEngine.UI.Image Image;
        public ImageConfigurationElement _Configuration;
        public ImageConfigurationElement Configuration
        {
            get { return _Configuration; }
            set { SetConfiguration(value); }
        }

        private SimpleButton button;

        private void SetConfiguration(ImageConfigurationElement value)
        {
            _Configuration = value;
            Invalidate();
        }

        protected override void CommitProperties()
        {
            base.CommitProperties();
            UpdateUI();
        }

        protected override void CollectComponents()
        {
            base.CollectComponents();

            button = GetComponentInChildren<SimpleButton>();
        }


        public override void AddListeners()
        {
            if (button != null)
                button.OnClick += onSelectButtonClicked;
        }

        public override void RemoveListeners()
        {
            if (button != null)
                button.OnClick -= onSelectButtonClicked;
        }

        private void onSelectButtonClicked()
        {
            SettingViewModel.TileImage = Configuration.Image;
        }

        private void UpdateUI()
        {
            if (Configuration != null)
            {
                Image.sprite = Sprite.Create(Configuration.Image, new Rect(0, 0, Configuration.Image.width, Configuration.Image.height), new Vector2(0.5f, 0.5f), 100.0f);
                Label.text = Configuration.Heading;
            }
        }
    }
}
