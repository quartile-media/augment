﻿using System;
using UI.Common.Components;
using UI.Settings.Image.Data;
using UnityEngine;

namespace UI.Settings.Image.Components
{
    public class ImageContainerComponent : UIComponent
    {
        public ImageConfiguration Database;
        public ImagePrototypeComponent Prototype;
        public GameObject SurfaceCover;

        protected override void OnStart()
        {
            base.OnStart();
            Spawn();
        }

        private void Spawn()
        {
            var siblingIndex = 1;
            foreach (var image in Database.Images)
            {
                var element = Instantiate(Prototype, transform);
                element.Configuration = image;
                element.transform.SetSiblingIndex(siblingIndex);
                siblingIndex++;
            }
            SurfaceCover.transform.SetSiblingIndex(siblingIndex);
        }
    }
}
