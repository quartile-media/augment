﻿using Model;
using System;
using System.Collections.Generic;
using UI.Common.Components;
using UI.Common.Data;
using UI.Common.Types;
using UI.Common.Views;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Settings.Image.Views
{
    public class SettingView : View
    {
        public Dropdown color;
        public Dropdown prototype;
        private List<SystemColor> colors = new List<SystemColor>();
        public MenuConfiguration PrototypeConfiguration;
        private SimpleButton button;

        protected override void CollectComponents()
        {
            button = GetComponentInChildren<SimpleButton>();
        }

        public override void AddListeners()
        {
            if (color != null)
            {
                color.onValueChanged.AddListener(onColorValueChanged);
            }

            if (prototype != null)
            {
                prototype.onValueChanged.AddListener(onPrototypeChanged);
            }

            if (button != null)
            {
                button.OnClick += onBackbuttonClicked;
            }
        }

        private void onBackbuttonClicked()
        {
            SceneManager.LoadScene("furniture");
            //SceneManager.LoadScene("ObjectManipulation");
        }

        protected override void OnStart()
        {
            SpawnColors();
            SpawnPrototypes();
        }

        private void SpawnPrototypes()
        {
            prototype.options.Clear();
            foreach (var prototypeItem in PrototypeConfiguration.Prototypes)
            {
                prototype.options.Add(new Dropdown.OptionData(prototypeItem.Heading));
            }
        }

        private void SpawnColors()
        {
            color.options.Clear();
            colors.Add(new SystemColor(Color.clear, "Clear"));
            colors.Add(new SystemColor(Color.black, "Black"));
            colors.Add(new SystemColor(Color.blue, "Blue"));
            colors.Add(new SystemColor(Color.cyan, "Cyan"));
            colors.Add(new SystemColor(Color.gray, "Gray"));
            colors.Add(new SystemColor(Color.green, "Green"));
            colors.Add(new SystemColor(Color.grey, "Grey"));
            colors.Add(new SystemColor(Color.magenta, "Magenta"));
            colors.Add(new SystemColor(Color.red, "Red"));
            colors.Add(new SystemColor(Color.white, "White"));
            colors.Add(new SystemColor(Color.yellow, "Yellow"));
            foreach (var colorItem in colors)
                color.options.Add(new Dropdown.OptionData(colorItem.Name));
        }

        private void onPrototypeChanged(int arg0)
        {
            SettingViewModel.PrototypeSelected = PrototypeConfiguration.Prototypes[arg0];
        }

        private void onColorValueChanged(int arg0)
        {
            SettingViewModel.SelectedColor = colors[arg0].Color;
        }

        public override void RemoveListeners()
        {
            if (color != null)
            {
                color.onValueChanged.RemoveAllListeners();
            }

            if (prototype != null)
            {
                prototype.onValueChanged.RemoveAllListeners();
            }
        }
    }
}