﻿using UnityEngine;

namespace UI.Settings.Image.Data
{
    [CreateAssetMenu(fileName = "ObjectPrototype", menuName = "RabbitEar/UI/Common/Data/Image database element", order = 1)]
    public class ImageConfigurationElement : ScriptableObject
    {
        public Texture2D Image;
        public string Heading;
    }
}
