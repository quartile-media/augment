﻿using UnityEngine;

namespace UI.Settings.Image.Data
{
    [CreateAssetMenu(fileName = "ObjectPrototype", menuName = "RabbitEar/UI/Common/Data/Image database", order = 1)]
    public class ImageConfiguration : ScriptableObject
    {
        public ImageConfigurationElement[] Images;
    }
}
